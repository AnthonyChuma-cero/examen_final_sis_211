/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.examen_final_sis_211;

public class Estudiantes_UATF {
    String Name;
    int Age;
    String Race;

    public Estudiantes_UATF() {
    }
    
    public Estudiantes_UATF(String Name, int Age, String Race) {
        this.Name = Name;
        this.Age = Age;
        this.Race = Race;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int Age) {
        this.Age = Age;
    }

    public String getRace() {
        return Race;
    }

    public void setRace(String Race) {
        this.Race = Race;
    }

    @Override
    public String toString() {
        return "Estudiantes_UATF{" + "Name=" + Name + ", Age=" + Age + ", Race=" + Race + '}';
    }
    
}
