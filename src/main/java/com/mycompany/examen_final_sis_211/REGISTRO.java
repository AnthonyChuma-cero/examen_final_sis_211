/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package com.mycompany.examen_final_sis_211;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author PERSONAL
 */
public class REGISTRO extends javax.swing.JFrame {

    /**
     * Creates new form REGISTRO
     */
    DefaultTableModel model = new DefaultTableModel();
    ArrayList<Estudiantes_UATF> List_est_uatf = new  ArrayList<Estudiantes_UATF>();
    
    public REGISTRO() {
        initComponents();
        this.setTitle("REGISTRO DE EST. DE UATF");
        this.setSize(460,600);
        this.setLocationRelativeTo(null);
        model.addColumn("NOMBRE_EST");
        model.addColumn("EDAD_EST");
        model.addColumn("CARRERA_EST");
        Registrtro();
    }

    @SuppressWarnings("unchecked")
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        EstRegistrados = new javax.swing.JTable();
        Nombre = new javax.swing.JLabel();
        edad = new javax.swing.JLabel();
        carrera = new javax.swing.JLabel();
        TexName = new javax.swing.JTextField();
        AgeEst = new javax.swing.JTextField();
        RaceEst = new javax.swing.JTextField();
        RegistrarEst = new javax.swing.JButton();
        Eliminar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        EstRegistrados.setBackground(new java.awt.Color(153, 255, 204));
        EstRegistrados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(EstRegistrados);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 251, 430, 269));

        Nombre.setBackground(new java.awt.Color(0, 0, 255));
        Nombre.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        Nombre.setText("Name :");
        getContentPane().add(Nombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 20, 90, 20));

        edad.setBackground(new java.awt.Color(0, 0, 255));
        edad.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        edad.setText("Age :");
        getContentPane().add(edad, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 60, 90, 20));

        carrera.setBackground(new java.awt.Color(0, 0, 255));
        carrera.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        carrera.setText("Race :");
        getContentPane().add(carrera, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 100, 100, 20));

        TexName.setBackground(new java.awt.Color(255, 255, 204));
        TexName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TexNameActionPerformed(evt);
            }
        });
        getContentPane().add(TexName, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 20, 300, -1));

        AgeEst.setBackground(new java.awt.Color(255, 255, 204));
        AgeEst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AgeEstActionPerformed(evt);
            }
        });
        getContentPane().add(AgeEst, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 60, 300, -1));

        RaceEst.setBackground(new java.awt.Color(255, 255, 204));
        RaceEst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RaceEstActionPerformed(evt);
            }
        });
        getContentPane().add(RaceEst, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 100, 300, -1));

        RegistrarEst.setBackground(new java.awt.Color(0, 204, 204));
        RegistrarEst.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        RegistrarEst.setText("Register");
        RegistrarEst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RegistrarEstActionPerformed(evt);
            }
        });
        getContentPane().add(RegistrarEst, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 150, -1, -1));

        Eliminar.setBackground(new java.awt.Color(0, 204, 204));
        Eliminar.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        Eliminar.setText("Delete");
        Eliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EliminarActionPerformed(evt);
            }
        });
        getContentPane().add(Eliminar, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 150, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void AgeEstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AgeEstActionPerformed
        String ageText = AgeEst.getText();
        JOptionPane.showMessageDialog(this, "La edad ingresada es: " + ageText);
    }//GEN-LAST:event_AgeEstActionPerformed

    private void RegistrarEstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RegistrarEstActionPerformed
        String nombre = TexName.getText();
        String edadStr = AgeEst.getText();
        String carrera = RaceEst.getText();

        if(nombre.isEmpty() || edadStr.isEmpty() || carrera.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Por favor llene el formulario.", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        try {
            int edad = Integer.parseInt(edadStr);
            Estudiantes_UATF estudiante = new Estudiantes_UATF(nombre, edad, carrera);
            List_est_uatf.add(estudiante);
            model.addRow(new Object[]{nombre, edad, carrera});
            JOptionPane.showMessageDialog(this, "Estudiante registrado.", "Registro Exitoso", JOptionPane.INFORMATION_MESSAGE);
            limpiarCampos();
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(this, "La edad debe ser en numeros.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_RegistrarEstActionPerformed

    private void EliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EliminarActionPerformed
        int filSeleccion = EstRegistrados.getSelectedRow();
        if (filSeleccion == -1){
            JOptionPane.showMessageDialog(this, "Selecione un estudiante. ", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        model.removeRow(filSeleccion);
        List_est_uatf.remove(filSeleccion);
        JOptionPane.showMessageDialog(this, "Estudiante eliminado ", " Eliminacion Exitosa", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_EliminarActionPerformed

    private void TexNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TexNameActionPerformed
        RegistrarEstActionPerformed(evt);
        if(contieneNumeros( TexName.getText())){
            JOptionPane.showMessageDialog(this, "El nombre no puede contener numeros.", "Error", JOptionPane.ERROR_MESSAGE);
            TexName.setText("");
        }else{
            RegistrarEstActionPerformed(evt);
        }
    }//GEN-LAST:event_TexNameActionPerformed

    private void RaceEstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RaceEstActionPerformed
        String race = RaceEst.getText();
        if(contieneNumeros(race)){
            JOptionPane.showMessageDialog(this, "La carrera no puede contener numeros.", "Error", JOptionPane.ERROR_MESSAGE);
            RaceEst.setText("");
        }else{
            JOptionPane.showMessageDialog(this, "La carrera seleccionada es: " + race);
        }
    }//GEN-LAST:event_RaceEstActionPerformed

    /**
     * @param args the command line arguments
     */
    public void Registrtro(){
        EstRegistrados.setModel(model);
    }
    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new REGISTRO().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField AgeEst;
    private javax.swing.JButton Eliminar;
    private javax.swing.JTable EstRegistrados;
    private javax.swing.JLabel Nombre;
    private javax.swing.JTextField RaceEst;
    private javax.swing.JButton RegistrarEst;
    private javax.swing.JTextField TexName;
    private javax.swing.JLabel carrera;
    private javax.swing.JLabel edad;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables

    private void limpiarCampos() {
        TexName.setText("");
        AgeEst.setText("");
        RaceEst.setText("");
    }
    private boolean contieneNumeros(String str) {
        return str.matches(".*\\d.*");
    }
}